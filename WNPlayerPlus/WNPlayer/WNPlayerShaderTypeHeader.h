//
//  WNPlayerShaderTypeHeader.h
//  Pods
//
//  Created by ilongge on 2023/8/1.
//

#ifndef WNPlayerShaderTypeHeader_h
#define WNPlayerShaderTypeHeader_h

#include <simd/simd.h>
 
// 存储数据的自定义结构，用于桥接 OC 和 Metal 代码（顶点）。
typedef struct {
    // 顶点坐标，4 维向量。
    vector_float4 position;
    // 纹理坐标。
    vector_float2 textureCoordinate;
} WNPlayerVertex;
 
// 存储数据的自定义结构，用于桥接 OC 和 Metal 代码（顶点）。
typedef struct {
    // YUV 矩阵。
    matrix_float3x3 matrix;
    // 是否为 full range。
    bool fullRange;
} WNPlayerConvertMatrix;
 
// 自定义枚举，用于桥接 OC 和 Metal 代码（顶点）。
// 顶点的桥接枚举值 WNPlayerVertexInputIndexVertices。
typedef enum WNPlayerVertexInputIndex {
    WNPlayerVertexInputIndexVertices = 0,
} WNPlayerVertexInputIndex;
 
// 自定义枚举，用于桥接 OC 和 Metal 代码（片元）。
// YUV 矩阵的桥接枚举值 WNPlayerFragmentInputIndexMatrix。
typedef enum WNPlayerFragmentBufferIndex {
    WNPlayerFragmentInputIndexMatrix = 0,
} WNPlayerMetalFragmentBufferIndex;
 
// 自定义枚举，用于桥接 OC 和 Metal 代码（片元）。
// YUV 数据的桥接枚举值 WNPlayerFragmentTextureIndexTextureY、WNPlayerFragmentTextureIndexTextureUV。
typedef enum WNPlayerFragmentYUVTextureIndex {
    WNPlayerFragmentTextureIndexTextureY = 0,
    WNPlayerFragmentTextureIndexTextureUV = 1,
} WNPlayerFragmentYUVTextureIndex;
 
// 自定义枚举，用于桥接 OC 和 Metal 代码（片元）。
// RGBA 数据的桥接枚举值 WNPlayerFragmentTextureIndexTextureRGB。
typedef enum WNPlayerFragmentRGBTextureIndex {
    WNPlayerFragmentTextureIndexTextureRGB = 0,
} WNPlayerFragmentRGBTextureIndex;
 
#endif /* WNPlayerShaderTypeHeader_h */
