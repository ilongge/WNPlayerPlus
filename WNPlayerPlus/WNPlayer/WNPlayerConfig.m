//
//  WNPlayerConfig.m
//  WNPlayerPlus
//
//  Created by ilongge on 2022/7/29.
//

#import "WNPlayerConfig.h"

@implementation WNPlayerConfig
- (instancetype)init
{
    self = [super init];
    if (self) {
        _hwaccel = NO;
        _useTCP = NO;
        _autoplay = NO;
        _repeat = NO;
        _preventFromScreenLock = NO;
        _restorePlayAfterAppEnterForeground = NO;
    }
    return self;
}

- (nonnull id)copyWithZone:(nullable NSZone *)zone
{
    WNPlayerConfig *config = [[WNPlayerConfig alloc] init];
    config.hwaccel = self.hwaccel;
    config.autoplay = self.autoplay;
    config.repeat = self.repeat;
    config.preventFromScreenLock = self.preventFromScreenLock;
    config.restorePlayAfterAppEnterForeground = self.restorePlayAfterAppEnterForeground;
    config.useTCP = self.useTCP;
    config.optionDic = [NSDictionary dictionaryWithDictionary:self.optionDic];
    return config;
}
@end
