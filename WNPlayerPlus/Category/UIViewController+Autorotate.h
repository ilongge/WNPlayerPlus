//
//  UIViewController+Autorotate.h
//  WNPlayer
//
//  Created by wenming on 2019/10/10.
//  Copyright © 2019 apple. All rights reserved.
//



#import <UIKit/UIKit.h>


@interface UIViewController (Autorotate)

@end

@interface UITabBarController (Autorotate)

@end

@interface UINavigationController (Autorotate)

@end

@interface UIAlertController (Autorotate)

@end

