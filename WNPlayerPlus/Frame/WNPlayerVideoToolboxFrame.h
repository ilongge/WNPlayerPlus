//
//  WNPlayerVideoToolboxFrame.h
//  WNPlayerPlus
//
//  Created by ilongge on 2023/7/31.
//

#import "WNPlayerVideoFrame.h"

@interface WNPlayerVideoToolboxFrame : WNPlayerVideoFrame
@property (nonatomic,assign) CVPixelBufferRef pixelBufferRef;
@end


