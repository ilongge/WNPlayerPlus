//
//  WNPlayerFrame.h
//  PlayerDemo
//
//  Created by zhengwenming on 2018/10/15.
//  Copyright © 2018年 wenming. All rights reserved.
//

@class WNPlayerConfig;
@class WNDisplayView;

#import <Foundation/Foundation.h>

typedef void (^onPauseComplete)(void);

@interface WNPlayerManager : NSObject

@property (nonatomic, strong) WNDisplayView *openGLESDisplayView;
@property (nonatomic, assign) double minBufferFrames;
@property (nonatomic, assign) double maxBufferFrames;
@property (nonatomic, assign) double position;
@property (nonatomic, assign) double duration;
@property (nonatomic, assign) BOOL opened;
@property (nonatomic, assign) BOOL playing;
@property (nonatomic, assign) BOOL buffering;
@property (nonatomic, assign) BOOL mute;
@property (nonatomic, strong) NSDictionary *metadata;

/**
 打开资源
 */
- (void)openSource:(NSString *)sourceUrl config:(WNPlayerConfig *)config;

/**
 * 关闭播放器
 */
- (void)close;
/**
 * 开始播放
 */
- (void)play;
/**
 * 暂停
 */
- (void)pause;
/**
 * 静音 OR 播放
 */
- (BOOL)muteVoice;
@end

