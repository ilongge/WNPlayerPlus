//
//  WNPlayerVideoFrame.h
//  PlayerDemo
//
//  Created by zhengwenming on 2018/10/15.
//  Copyright © 2018年 wenming. All rights reserved.
//

#import "WNPlayerFrame.h"
#import <OpenGLES/ES2/gl.h>

typedef enum : NSUInteger {
    kWNPlayerVideoFrameTypeNone,
    kWNPlayerVideoFrameTypeRGB,
    kWNPlayerVideoFrameTypeYUV
} WNPlayerVideoFrameType;


@interface WNPlayerVideoFrame : WNPlayerFrame
@property (nonatomic) WNPlayerVideoFrameType videoType;
@property (nonatomic) int width;
@property (nonatomic) int height;

- (BOOL)prepareRender:(GLuint)program;
@end

