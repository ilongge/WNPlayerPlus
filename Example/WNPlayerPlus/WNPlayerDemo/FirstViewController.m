//
//  FirstViewController.m
//  WNPlayer
//
//  Created by apple on 2019/10/10.
//  Copyright © 2019 apple. All rights reserved.
//

#import "FirstViewController.h"
#import "DetailViewController.h"

@interface FirstViewController ()
@property(nonatomic,strong)UIButton *softwarebtn;
@property(nonatomic,strong)UIButton *hardwarebtn;

@end

@implementation FirstViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.softwarebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.softwarebtn.frame = CGRectMake(0, 0, 150, 50);
    [self.softwarebtn setTitle:@"软解" forState:UIControlStateNormal];
    self.softwarebtn.backgroundColor = UIColor.grayColor;
    [self.softwarebtn addTarget:self action:@selector(pushDetailVCSoftware:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: self.softwarebtn];
    
    self.hardwarebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.hardwarebtn.frame = CGRectMake(0, 0, 150, 50);
    [self.hardwarebtn setTitle:@"硬解" forState:UIControlStateNormal];
    self.hardwarebtn.backgroundColor = UIColor.grayColor;
    [self.hardwarebtn addTarget:self action:@selector(pushDetailVCHardware:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview: self.hardwarebtn];
}

-(void)pushDetailVCSoftware:(UIButton *)sender
{
    DetailViewController *detailVC = [DetailViewController new];
    detailVC.videoToolbox = NO;
    [self.navigationController pushViewController:detailVC animated:YES];
}

-(void)pushDetailVCHardware:(UIButton *)sender
{
    DetailViewController *detailVC = [DetailViewController new];
    detailVC.videoToolbox = YES;
    [self.navigationController pushViewController:detailVC animated:YES];
}

-(void)viewWillLayoutSubviews{
    self.softwarebtn.center = CGPointMake(self.view.center.x, self.view.center.y - 30);
    self.hardwarebtn.center = CGPointMake(self.view.center.x, self.view.center.y + 30);
}
@end
