//
//  WNPlayerFrame.h
//  PlayerDemo
//
//  Created by zhengwenming on 2018/10/15.
//  Copyright © 2018年 wenming. All rights reserved.
//

@class WNPlayerConfig;

#import <Foundation/Foundation.h>

@interface WNPlayerDecoder : NSObject
@property (nonatomic) BOOL hasVideo;
@property (nonatomic) BOOL hasAudio;
@property (nonatomic) BOOL hasPicture;
@property (nonatomic) BOOL isEOF;

@property (nonatomic) double rotation;
@property (nonatomic) double duration;
@property (nonatomic, strong) NSDictionary *metadata;

@property (nonatomic) UInt32 audioChannels;
@property (nonatomic) float audioSampleRate;

@property (nonatomic) double videoFPS;
@property (nonatomic) double videoTimebase;
@property (nonatomic) double audioTimebase;
/**
 * 色彩空间
 */
@property (nonatomic, copy) NSString *color_space;
/**
 * 色彩范围
 */
@property (nonatomic, copy) NSString *color_range;

- (BOOL)openSource:(NSString *)url config:(WNPlayerConfig *)config error:(NSError **)error;

- (void)close;

- (void)prepareClose;

- (NSArray *)readFrames;

- (void)seek:(double)position;

- (int)videoWidth;

- (int)videoHeight;
/**
 * 配置
 */
- (WNPlayerConfig *)config;
@end

