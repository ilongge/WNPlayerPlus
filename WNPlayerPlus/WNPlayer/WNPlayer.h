//
//  WNPlayerFrame.h
//  PlayerDemo
//
//  Created by zhengwenming on 2018/10/15.
//  Copyright © 2018年 wenming. All rights reserved.
//

@class WNPlayer;
@class WNPlayerManager;
@class WNPlayerConfig;

#import <UIKit/UIKit.h>
#import "WNControlViewProtocol.h"

typedef NS_ENUM(NSInteger,WNPlayerStatus) {
    WNPlayerStatusNone,
    WNPlayerStatusOpening,
    WNPlayerStatusOpened,
    WNPlayerStatusPlaying,
    WNPlayerStatusBuffering,
    WNPlayerStatusPaused,
    WNPlayerStatusEOF,
    WNPlayerStatusClosing,
    WNPlayerStatusClosed,
}; 

@protocol WNPlayerDelegate <NSObject>
@optional
/**
 * 点击播放暂停按钮代理方法
 */
- (void)player:(WNPlayer *_Nullable)player clickedPlayOrPauseButton:(UIButton *_Nullable)playOrPauseBtn;
/**
 * 点击关闭按钮代理方法
 */
- (void)player:(WNPlayer *_Nullable)player clickedCloseButton:(UIButton *_Nonnull)backBtn;
/**
 * 点击全屏按钮代理方法
 */
- (void)player:(WNPlayer *_Nonnull)player clickedFullScreenButton:(UIButton *_Nonnull)fullScreenBtn;
/**
 * 单击WMPlayer的代理方法
 */
- (void)player:(WNPlayer *_Nullable)player singleTaped:(UITapGestureRecognizer *_Nullable)singleTap;
/**
 * 双击WMPlayer的代理方法
 */
- (void)player:(WNPlayer *_Nullable)player doubleTaped:(UITapGestureRecognizer *_Nullable)doubleTaped;
/**
 * 播放失败的代理方法
 */
- (void)playerFailedPlay:(WNPlayer *_Nullable)player error:(NSError *_Nullable)error;
/**
 * 播放器已经拿到视频的尺寸大小
 */
- (void)playerReadyToPlay:(WNPlayer *_Nullable)player videoSize:(CGSize )presentationSize;
/**
 * 播放完毕的代理方法
 */
- (void)playerFinishedPlay:(WNPlayer *_Nullable)player;
@end



NS_ASSUME_NONNULL_BEGIN

@interface WNPlayer : UIView
/**
 * 播放链接
 */
@property (nonatomic, copy) NSString *urlString;
/**
 * WNPlayerDelegate委托代理
 */
@property (nonatomic, weak)id <WNPlayerDelegate> delegate;
/**
 * 控制层
 * 设置控制层，如果不设置这个controlView，则没有控制层，只有视频画面
 * 开发者可自定义一个UIView
 * 遵守WNControlViewProtocol
 * 添加自己的子控件
 * 控件的事件连接WNControlViewProtocol的事件
 */
@property (nonatomic, strong) UIView <WNControlViewProtocol> *controlView;
/**
 * 播放器管理器
 */
@property (nonatomic, strong) WNPlayerManager *playerManager;

@property (nonatomic, assign) BOOL isFullScreen;
/**
 * 获取当前视频播放帧的截图UIImage
 */
- (UIImage*)snapshot:(CGSize)viewSize;
/**
 * 开始播放
 */
- (void)play;
/**
 * 暂停
 */
- (void)pause;
/**
 * 关闭播放器
 */
- (void)close;
/**
 * 播放器状态
 */
- (WNPlayerStatus)status;
/**
 * 更新配置
 */
- (void)setConfig:(WNPlayerConfig *)config;
/**
 * 配置
 */
- (WNPlayerConfig *)config;
@end

NS_ASSUME_NONNULL_END
