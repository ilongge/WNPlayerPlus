//
//  WNPlayerVideoRGBFrame.h
//  PlayerDemo
//
//  Created by zhengwenming on 2018/10/15.
//  Copyright © 2018年 wenming. All rights reserved.
//

#import "WNPlayerVideoFrame.h"

@interface WNPlayerVideoRGBFrame : WNPlayerVideoFrame
@property (nonatomic) NSUInteger linesize;
@property (nonatomic) BOOL hasAlpha;
@end

