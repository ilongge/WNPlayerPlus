//
//  WNPlayerFrame.h
//  PlayerDemo
//
//  Created by zhengwenming on 2018/10/15.
//  Copyright © 2018年 wenming. All rights reserved.
//


#import "WNPlayerManager.h"
#import "WNPlayerDecoder.h"
#import "WNPlayerDef.h"
#import "WNAudioManager.h"
#import "WNPlayerFrame.h"
#import "WNPlayerVideoFrame.h"
#import "WNPlayerAudioFrame.h"
#import "WNPlayerVideoToolboxFrame.h"
#import "WNDisplayView.h"
#import "WNPlayerConfig.h"

@interface WNPlayerManager () {
    
}
@property (nonatomic, strong) WNPlayerDecoder *decoder;
@property (nonatomic, strong) WNAudioManager *audioManager;
@property (nonatomic, strong) NSMutableArray *videoFrameList;
@property (nonatomic, strong) NSMutableArray *audioFrameList;
@property (nonatomic, strong) WNPlayerAudioFrame *playingAudioFrame;
@property (nonatomic, assign) NSUInteger playingAudioFrameDataPosition;
@property (nonatomic, assign) double bufferedFrames;
@property (nonatomic, assign) double mediaPosition;
@property (nonatomic, assign) double mediaSyncTime;
@property (nonatomic, assign) double mediaSyncPosition;
@property (nonatomic, assign) double maxEndTime;
@property (nonatomic, strong) NSThread *frameReaderThread;
@property (nonatomic, assign) BOOL notifiedBufferStart;
@property (nonatomic, assign) BOOL requestSeek;
@property (nonatomic, assign) double requestSeekPosition;
@property (nonatomic, assign) BOOL opening;
@property (nonatomic, strong) dispatch_semaphore_t vFramesLock;
@property (nonatomic, strong) dispatch_semaphore_t aFramesLock;
@property (nonatomic, strong) NSMutableArray *tempVFrames;
@property (nonatomic, strong) NSMutableArray *tempAFrames;
@property (nonatomic, assign) NSInteger tempFrame;
@property (nonatomic, assign) dispatch_time_t t;
/**
 * 播放配置
 */
@property (nonatomic, strong) WNPlayerConfig *config;
@end

@implementation WNPlayerManager
-(void)setMediaPosition:(double)mediaPosition
{
    _mediaPosition = mediaPosition;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [self initVars];
        self.audioManager = [[WNAudioManager alloc] init];
        self.decoder = [[WNPlayerDecoder alloc] init];
        self.openGLESDisplayView = [[WNDisplayView alloc] init];
    }
    return self;
}

- (void)initVars {
    self.minBufferFrames = WNPlayerMinBufferFrames;
    self.maxBufferFrames = WNPlayerMaxBufferFrames;
    self.bufferedFrames = 0;
    self.mediaPosition = 0;
    self.mediaSyncTime = 0;
    self.videoFrameList = [NSMutableArray arrayWithCapacity:128];
    self.audioFrameList = [NSMutableArray arrayWithCapacity:128];
    self.playingAudioFrame = nil;
    self.playingAudioFrameDataPosition = 0;
    self.opening = NO;
    self.buffering = NO;
    self.playing = NO;
    self.opened = NO;
    self.requestSeek = NO;
    self.requestSeekPosition = 0;
    self.frameReaderThread = nil;
    self.aFramesLock = dispatch_semaphore_create(1);
    self.vFramesLock = dispatch_semaphore_create(1);
}

- (void)clearVars {
    [self.videoFrameList removeAllObjects];
    [self.audioFrameList removeAllObjects];
    self.audioManager = nil;
    self.playingAudioFrame = nil;
    self.playingAudioFrameDataPosition = 0;
    self.opening = NO;
    self.buffering = NO;
    self.playing = NO;
    self.opened = NO;
    self.bufferedFrames = 0;
    self.mediaPosition = 0;
    self.mediaSyncTime = 0;
    [self.openGLESDisplayView clear];
}

#pragma mark ---------------------------- Public

- (void)openSource:(NSString *)sourceUrl config:(WNPlayerConfig *)config
{
    self.config = config;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSError *error = nil;
        self.opening = YES;
        if ([self.audioManager open:&error]) {
            self.decoder.audioChannels = [self.audioManager channels];
            self.decoder.audioSampleRate = [self.audioManager sampleRate];
        }
        else {
            [self handleError:error];
        }
        BOOL ret = [self.decoder openSource:sourceUrl config:config error:&error];
        if (ret == NO) {
            self.opening = NO;
            [self handleError:error];
            return;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            self.openGLESDisplayView.keepLastFrame = [self.decoder hasPicture] && ![self.decoder hasVideo];
            self.openGLESDisplayView.rotation = self.decoder.rotation;
            self.openGLESDisplayView.contentSize = CGSizeMake([self.decoder videoWidth], [self.decoder videoHeight]);
            self.openGLESDisplayView.contentMode = UIViewContentModeScaleAspectFit;
            self.duration = self.decoder.duration;
            self.metadata = self.decoder.metadata;
            self.opening = NO;
            self.buffering = NO;
            self.playing = NO;
            self.bufferedFrames = 0;
            self.mediaPosition = 0;
            self.mediaSyncTime = 0;
            __weak typeof(self)weakSelf = self;
            self.audioManager.frameReaderBlock = ^(float *data, UInt32 frames, UInt32 channels) {
                [weakSelf readAudioFrame:data frames:frames channels:channels];
            };
            self.opened = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:WNPlayerNotificationOpened object:self];
        });
    });
}
- (void)close {
    if (!self.opened && !self.opening) {
        [[NSNotificationCenter defaultCenter] postNotificationName:WNPlayerNotificationClosed object:self];
        return;
    }
    
    [self pause];
    [self.decoder prepareClose];
    
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC, 0.1 * NSEC_PER_SEC);
    
    dispatch_source_set_event_handler(timer, ^{
        if (self.opening || self.buffering) {
            self.buffering = NO;
            [self.audioFrameList removeAllObjects];
            [self.videoFrameList removeAllObjects];
        }
        [self.decoder close];
        
        NSArray<NSError *> *errors = nil;
        if ([self.audioManager close:&errors]) {
            [self clearVars];
            [[NSNotificationCenter defaultCenter] postNotificationName:WNPlayerNotificationClosed object:self];
        }
        else {
            for (NSError *error in errors) {
                [self handleError:error];
            }
        }
        dispatch_cancel(timer);
    });
    dispatch_resume(timer);
}

- (void)play {
    if (!self.opened || self.playing) {
        return;
    }
    self.playing = YES;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1)), dispatch_get_main_queue(), ^{
        [self startFrameReaderThread];
        [self renderFrames];
    });
    
    NSError *error = nil;
    if (![self.audioManager play:&error]) {
        [self handleError:error];
    }
}

- (void)pause {
    self.playing = NO;
    NSError *error = nil;
    if (![self.audioManager pause:&error]) {
        [self handleError:error];
    }
}

- (BOOL)muteVoice {
    NSError *error = nil;
    BOOL ret = [self.audioManager mute:!_mute error:&error];
    if (!ret) {
        [self handleError:error];
    }
    else{
        _mute = !_mute;
    }
    return _mute;
}

#pragma mark ---------------------------- Private

- (void)startFrameReaderThread {
    if (self.frameReaderThread == nil) {
        self.frameReaderThread = [[NSThread alloc] initWithTarget:self selector:@selector(runFrameReader) object:nil];
        [self.frameReaderThread start];
    }
}

- (void)runFrameReader {
    @autoreleasepool {
        while (self.playing) {
            [self readUncompressFrames];
            if (self.requestSeek) {
                [self seekPositionInFrameReader];
            }
            else {
                [NSThread sleepForTimeInterval:5];
            }
        }
        self.frameReaderThread = nil;
    }
}
- (void)readUncompressFrames {
    
    self.buffering = YES;
    
    [self.tempAFrames removeAllObjects];
    [self.tempVFrames removeAllObjects];
    
    if (self.tempVFrames == nil) {
        self.tempVFrames = [NSMutableArray array];
    }
    if (self.tempAFrames == nil) {
        self.tempAFrames = [NSMutableArray array];
    }
    
    self.tempFrame = 0;
    
    self.t = dispatch_time(DISPATCH_TIME_NOW, 0.02 * NSEC_PER_SEC);
    
    while (true) {
        if (self.playing == NO) {
            [NSThread sleepForTimeInterval:5];
        }
        else if(self.decoder.isEOF == YES)
        {
            WNPlayerFrame *vframe;
            WNPlayerFrame *aframe;
            NSLock *lock = [[NSLock alloc] init];
            [lock lock];
            NSInteger videoCount = self.videoFrameList.count-2;
            if (videoCount > 0) {
                vframe = self.videoFrameList[videoCount];
            }
            NSInteger audioCount = self.audioFrameList.count-2;
            if (audioCount > 0) {
                aframe = self.audioFrameList[audioCount];
            }
           
            if (vframe || aframe){
                self.maxEndTime = aframe.position > vframe.position ? aframe.position : vframe.position;
            }
            if (self.mediaPosition >= self.maxEndTime){
                self.opened = NO;
                self.opening = NO;
                [self close];
                break;
            }
            double time = self.bufferedFrames / self.decoder.videoFPS * 0.5;
            [NSThread sleepForTimeInterval:time];
            [lock unlock];
        }
        else if(self.requestSeek == YES)
        {
            [NSThread sleepForTimeInterval:1];
        }
        else if((self.bufferedFrames + self.tempFrame) >= self.maxBufferFrames)
        {
            double time = self.bufferedFrames / self.decoder.videoFPS * 0.5;
            [NSThread sleepForTimeInterval:time];
        }
        else {
            @autoreleasepool {
                NSArray *fremes = [self.decoder readFrames];
                if (fremes == nil) {
                    break;
                }
                if (fremes.count == 0) {
                    continue;
                }
                {
                    for (WNPlayerFrame *frame in fremes) {
                        if (frame.type == kWNPlayerFrameTypeVideo) {
                            [self.tempVFrames addObject:frame];
                            self.tempFrame += 1;
                        }
                    }
                    
                    long timeout = dispatch_semaphore_wait(self.vFramesLock, self.t);
                    if (timeout == 0) {
                        if (self.tempVFrames.count > 0) {
                            self.bufferedFrames += self.tempFrame;
                            self.tempFrame = 0;
                            [self.videoFrameList addObjectsFromArray:self.tempVFrames];
                            [self.tempVFrames removeAllObjects];
                        }
                        dispatch_semaphore_signal(self.vFramesLock);
                    }
                }
                {
                    for (WNPlayerFrame *frame in fremes) {
                        if (frame.type == kWNPlayerFrameTypeAudio) {
                            [self.tempAFrames addObject:frame];
                            if (!self.decoder.hasVideo) {
                                self.tempFrame += 1;
                            }
                        }
                    }
                    
                    long timeout = dispatch_semaphore_wait(self.aFramesLock, self.t);
                    if (timeout == 0) {
                        if (self.tempAFrames.count > 0) {
                            if (!self.decoder.hasVideo) {
                                self.bufferedFrames += self.tempFrame;
                                self.tempFrame = 0;
                            }
                            [self.audioFrameList addObjectsFromArray:self.tempAFrames];
                            [self.tempAFrames removeAllObjects];
                        }
                        dispatch_semaphore_signal(self.aFramesLock);
                    }
                }
            }
        }
    }
    
    {
        // add the rest video frames
        while (self.tempVFrames.count > 0 || self.tempAFrames.count > 0) {
            if (self.tempVFrames.count > 0) {
                long timeout = dispatch_semaphore_wait(self.vFramesLock, self.t);
                if (timeout == 0) {
                    self.bufferedFrames += self.tempFrame;
                    self.tempFrame = 0;
                    [self.videoFrameList addObjectsFromArray:self.tempVFrames];
                    [self.tempVFrames removeAllObjects];
                    dispatch_semaphore_signal(self.vFramesLock);
                }
            }
            if (self.tempAFrames.count > 0) {
                long timeout = dispatch_semaphore_wait(self.aFramesLock, self.t);
                if (timeout == 0) {
                    if (!self.decoder.hasVideo) {
                        self.bufferedFrames += self.tempFrame;
                        self.tempFrame = 0;
                    }
                    [self.audioFrameList addObjectsFromArray:self.tempAFrames];
                    [self.tempAFrames removeAllObjects];
                    dispatch_semaphore_signal(self.aFramesLock);
                }
            }
        }
    }
    self.buffering = NO;
}

- (void)seekPositionInFrameReader {
    [self.decoder seek:self.requestSeekPosition];
    {
        dispatch_semaphore_wait(self.vFramesLock, DISPATCH_TIME_FOREVER);
        [self.videoFrameList removeAllObjects];
        dispatch_semaphore_signal(self.vFramesLock);
    }
    {
        dispatch_semaphore_wait(self.aFramesLock, DISPATCH_TIME_FOREVER);
        [self.audioFrameList removeAllObjects];
        dispatch_semaphore_signal(self.aFramesLock);
    }
    
    self.bufferedFrames = 0;
    self.requestSeek = NO;
    self.mediaSyncTime = 0;
    self.mediaPosition = self.requestSeekPosition;
}

- (void)renderFrames {
    
    if (!self.playing) {
        return;
    }
    BOOL noframes = ((self.decoder.hasVideo && self.videoFrameList.count == 0)
                     &&
                     (self.decoder.hasAudio && self.audioFrameList.count == 0));
    // 解码完成且没有未渲染的帧
    if (self.decoder.isEOF && noframes){
        return;
    }
    if (noframes && !self.notifiedBufferStart) {
        self.notifiedBufferStart = YES;
        NSDictionary *userInfo = @{ WNPlayerNotificationBufferStateKey : @(self.notifiedBufferStart) };
        [[NSNotificationCenter defaultCenter] postNotificationName:WNPlayerNotificationBufferStateChanged object:self userInfo:userInfo];
    }
    else if (!noframes
             &&
             self.notifiedBufferStart
             &&
             self.bufferedFrames >= self.minBufferFrames) {
        self.notifiedBufferStart = NO;
        NSDictionary *userInfo = @{ WNPlayerNotificationBufferStateKey : @(self.notifiedBufferStart) };
        [[NSNotificationCenter defaultCenter] postNotificationName:WNPlayerNotificationBufferStateChanged object:self userInfo:userInfo];
    }
    
    // Render if has picture
    if (self.decoder.hasPicture && self.videoFrameList.count > 0) {
        WNPlayerVideoFrame *frame = self.videoFrameList[0];
        [self.videoFrameList removeObjectAtIndex:0];
        self.openGLESDisplayView.contentSize = CGSizeMake(frame.width, frame.height);
        if (frame) {
            [self.openGLESDisplayView renderVideoFrame:frame];
        }
    }
    
    // Check whether render is neccessary
    if (self.videoFrameList.count <= 0
        ||
        !self.decoder.hasVideo
        ||
        self.notifiedBufferStart) {
        __weak typeof(self)weakSelf = self;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf renderFrames];
        });
        return;
    }
    
    // Render video
    WNPlayerVideoFrame *frame = nil;
    {
        long timeout = dispatch_semaphore_wait(self.vFramesLock, DISPATCH_TIME_NOW);
        if (timeout == 0) {
            frame = self.videoFrameList[0];
            [self.videoFrameList removeObjectAtIndex:0];
            if (frame.position >= self.mediaPosition) {
                self.mediaPosition = frame.position;
            }
            self.bufferedFrames -= 1;
            dispatch_semaphore_signal(self.vFramesLock);
        }
    }
    if (frame) {
        [self.openGLESDisplayView renderVideoFrame:frame];
    }
    
    // Sync audio with video
    double syncTime = [self syncTime];
    NSTimeInterval t = MAX(frame.duration + syncTime, 0.01);
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(t * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self renderFrames];
    });
    
    if (self.videoFrameList.count <= 0
        &&
        self.audioFrameList.count <= 0
        &&
        self.decoder.isEOF == YES) {
        [self.openGLESDisplayView clear];
    }
}

- (double)syncTime {
    const double now = [NSDate timeIntervalSinceReferenceDate];
    if (self.mediaSyncTime == 0) {
        self.mediaSyncTime = now;
        self.mediaSyncPosition = self.mediaPosition;
        return 0;
    }
    double dp = self.mediaPosition - self.mediaSyncPosition;
    double dt = now - self.mediaSyncTime;
    double sync = dp - dt;
    if (sync > 1 || sync < -1) {
        sync = 0;
        self.mediaSyncTime = 0;
    }
    return sync;
}

/*
 * For audioUnitRenderCallback, (WNPlayerAudioManagerFrameReaderBlock)readFrameBlock
 */
- (void)readAudioFrame:(float *)data frames:(UInt32)frames channels:(UInt32)channels {
    
    if (!self.playing) {
        return;
    }
    while(frames > 0) {
        @autoreleasepool {
            if (self.playingAudioFrame == nil) {
                {
                    if (self.audioFrameList.count <= 0) {
                        memset(data, 0, frames * channels * sizeof(float));
                        return;
                    }
                    
                    long timeout = dispatch_semaphore_wait(self.aFramesLock, DISPATCH_TIME_NOW);
                    if (timeout == 0) {
                        WNPlayerAudioFrame *frame = self.audioFrameList[0];
                        if (self.decoder.hasVideo) {
                            const double dt = self.mediaPosition - frame.position;
                            if (dt < -0.1) { // audio is faster than video, silence
                                memset(data, 0, frames * channels * sizeof(float));
                                dispatch_semaphore_signal(self.aFramesLock);
                                break;
                            }
                            else if (dt > 0.1) { // audio is slower than video, skip
                                [self.audioFrameList removeObjectAtIndex:0];
                                dispatch_semaphore_signal(self.aFramesLock);
                                continue;
                            }
                            else {
                                self.playingAudioFrameDataPosition = 0;
                                self.playingAudioFrame = frame;
                                [self.audioFrameList removeObjectAtIndex:0];
                            }
                        }
                        else {
                            self.playingAudioFrameDataPosition = 0;
                            self.playingAudioFrame = frame;
                            [self.audioFrameList removeObjectAtIndex:0];
                            self.bufferedFrames -= 1;
                        }
                        if (frame.position >= self.mediaPosition) {
                            self.mediaPosition = frame.position;
                        }
                        dispatch_semaphore_signal(self.aFramesLock);
                    }
                    else {
                        return;
                    }
                }
            }
            
            NSData *frameData = self.playingAudioFrame.data;
            NSUInteger pos = self.playingAudioFrameDataPosition;
            if (frameData == nil) {
                memset(data, 0, frames * channels * sizeof(float));
                return;
            }
            
            const void *bytes = (Byte *)frameData.bytes + pos;
            const NSUInteger remainingBytes = frameData.length - pos;
            const NSUInteger channelSize = channels * sizeof(float);
            const NSUInteger bytesToCopy = MIN(frames * channelSize, remainingBytes);
            const NSUInteger framesToCopy = bytesToCopy / channelSize;
            
            memcpy(data, bytes, bytesToCopy);
            frames -= framesToCopy;
            data += framesToCopy * channels;
            
            if (bytesToCopy < remainingBytes) {
                self.playingAudioFrameDataPosition += bytesToCopy;
            }
            else {
                self.playingAudioFrame = nil;
            }
        }
    }
}

- (void)setPosition:(double)position {
    self.requestSeekPosition = position;
    self.requestSeek = YES;
}

- (double)position {
    return self.mediaPosition;
}

#pragma mark - Handle Error
- (void)handleError:(NSError *)error {
    if (error == nil) {
        return;
    }
    NSDictionary *userInfo = @{ WNPlayerNotificationErrorKey : error };
    [[NSNotificationCenter defaultCenter] postNotificationName:WNPlayerNotificationError object:self userInfo:userInfo];
}
- (void)dealloc {
    NSLog(@"%s",__FUNCTION__);
}
@end
