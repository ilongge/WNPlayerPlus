//
//  WNPlayerConfig.h
//  WNPlayerPlus
//
//  Created by ilongge on 2022/7/29.
//

#import <Foundation/Foundation.h>

@interface WNPlayerConfig : NSObject <NSCopying>
/**
 * 默认是UDP，如有需要用TCP，请传YES
 */
@property (nonatomic,assign) BOOL useTCP;
/**
 * 是否自动播放
 */
@property (nonatomic,assign) BOOL autoplay; 
/**
 * 是否重复播放
 */
@property (nonatomic,assign) BOOL repeat;
/**
 * 是否锁屏
 */
@property (nonatomic,assign) BOOL preventFromScreenLock;
/**
 * app进入前台后继续播放
 */
@property (nonatomic,assign) BOOL restorePlayAfterAppEnterForeground;
/**
 * optionDic里面可以设置key-value，比如headers-cookie：xxxx
 */
@property (nonatomic, strong) NSDictionary *optionDic;
/**
 * 是否开启硬件解码(仅当当前硬件支持当前视频编码格式时起效)
 */
@property (nonatomic,assign) BOOL hwaccel;
@end

