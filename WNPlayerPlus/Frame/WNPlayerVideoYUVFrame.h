//
//  WNPlayerVideoYUVFrame.h
//  PlayerDemo
//
//  Created by zhengwenming on 2018/10/15.
//  Copyright © 2018年 wenming. All rights reserved.
//

#import "WNPlayerVideoFrame.h"

@interface WNPlayerVideoYUVFrame : WNPlayerVideoFrame
@property (nonatomic, strong) NSData *Y;    // Luma
@property (nonatomic, strong) NSData *Cb;   // Chroma Blue
@property (nonatomic, strong) NSData *Cr;   // Chroma Red

@end

