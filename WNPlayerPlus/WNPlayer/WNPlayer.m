//
//  WNPlayerFrame.h
//  PlayerDemo
//
//  Created by zhengwenming on 2018/10/15.
//  Copyright © 2018年 wenming. All rights reserved.
//

#import "WNPlayer.h"
#import "WNPlayerUtils.h"
#import "WNPlayerDef.h"
#import "WNDisplayView.h"
#import "WNControlView.h"
#import "WNPlayerManager.h"
#import "DefineHeader.h"
#import "CategoryHeader.h"
#import "WNPlayerConfig.h"

typedef NS_ENUM(NSInteger, WNPlayerOperation) {
    WNPlayerOperationNone,
    WNPlayerOperationOpen,
    WNPlayerOperationPlay,
    WNPlayerOperationPause,
    WNPlayerOperationClose,
};

@interface WNPlayer ()
@property (nonatomic) dispatch_source_t dispath_timer;
@property (nonatomic,assign) BOOL restorePlay;
@property (nonatomic,assign) WNPlayerStatus status;
@property (nonatomic) WNPlayerOperation nextOperation;
@property (nonatomic, strong) UIView  *viewStatusBar;
/**
 * 播放配置
 */
@property (nonatomic, strong) WNPlayerConfig *config;
@end

@implementation WNPlayer

- (UIImage*)snapshot:(CGSize)viewSize{
    if (self.config.hwaccel) {
        return [WNDisplayView glToUIImage:self.playerManager.openGLESDisplayView.frame.size];
    }
    else{
        return [WNDisplayView glToUIImage:self.playerManager.openGLESDisplayView.frame.size];
    }
}
- (instancetype)init{
    self = [super init];
    if (self) {
        [self initSubViews];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubViews];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame controlView:(WNControlView *)controlView{
    self = [super initWithFrame:frame];
    if (self) {
        if (controlView) {
            _controlView = controlView;
            [self addSubview:_controlView];
        }
        [self initSubViews];
    }
    return self;
}

-(void)setControlView:(UIView<WNControlViewProtocol> *)controlView{
    _controlView = controlView;
    if (!controlView) {
        return;
    }
    controlView.player = self;
    [self addSubview:controlView];
}
-(void)initSubViews{
    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor blackColor];
    self.playerManager = [[WNPlayerManager alloc] init];
    self.status = WNPlayerStatusNone;
    self.nextOperation = WNPlayerOperationNone;    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground:)
                                                 name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:)
                                                 name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerOpened:) name:WNPlayerNotificationOpened object:self.playerManager];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerClosed:) name:WNPlayerNotificationClosed object:self.playerManager];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerPlayComplete:) name:WNPlayerNotificationPlayComplete object:self.playerManager];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerBufferStateChanged:) name:WNPlayerNotificationBufferStateChanged object:self.playerManager];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerError:) name:WNPlayerNotificationError object:self.playerManager];
}
- (UIView *)viewStatusBar{
    if (!_viewStatusBar) {
        if (@available(iOS 13.0, *)) {
            UIStatusBarManager *statusBarManager = [[UIApplication sharedApplication].keyWindow.windowScene statusBarManager];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wundeclared-selector"
            if ([statusBarManager respondsToSelector:NSSelectorFromString(@"createLocalStatusBar")]) {
                _viewStatusBar = [statusBarManager performSelector:NSSelectorFromString(@"createLocalStatusBar")];
            }
#pragma clang diagnostic pop
            _viewStatusBar.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
            _viewStatusBar.backgroundColor = [UIColor clearColor];
            _viewStatusBar.hidden = YES;
            _viewStatusBar.tag = 321;
            //手动设置状态栏字体颜色为白色
            [[[_viewStatusBar valueForKey:@"_statusBar"]valueForKey:@"_statusBar"] performSelector:@selector(setForegroundColor:) withObject:[UIColor whiteColor]];
        }
        else {
            // Fallback on earlier versions
        }
        
    }
    return _viewStatusBar;
}
- (WNPlayerStatus)status {
    return _status;
}
#pragma mark ---------------------------- layoutSubviews
-(void)layoutSubviews{
    [super layoutSubviews];
    self.playerManager.openGLESDisplayView.frame = self.bounds;
    if (self.isFullScreen) {
        self.controlView.frame = CGRectMake(60, 0, self.frame.size.width-2*60, self.frame.size.height);
    }
    else{
        self.controlView.frame = self.bounds;
    }
}

#pragma mark ---------------------------- Public

-(void)syncScrubber{
    if (!self.playerManager.playing) {
        return;
    }
    double position = self.playerManager.position;
    if (self.controlView&&[self.controlView respondsToSelector:@selector(syncScrubber:)]) {
        [self.controlView performSelector:@selector(syncScrubber:) withObject:@(position)];
    }
}

- (void)play {
    if (self.status == WNPlayerStatusNone ||
        self.status == WNPlayerStatusClosed) {
        [self openWithConfig:self.config];
        self.nextOperation = WNPlayerOperationPlay;
    }
    if (self.status != WNPlayerStatusOpened &&
        self.status != WNPlayerStatusPaused &&
        self.status != WNPlayerStatusEOF) {
        return;
    }
    self.status = WNPlayerStatusPlaying;
    [UIApplication sharedApplication].idleTimerDisabled = self.config.preventFromScreenLock;
    [self.playerManager play];
}

- (void)replay {
    self.playerManager.position = 0;
    [self play];
}

- (void)pause {
    if (self.status != WNPlayerStatusOpened &&
        self.status != WNPlayerStatusPlaying &&
        self.status != WNPlayerStatusEOF) {
        return;
    }
    self.status = WNPlayerStatusPaused;
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    [self.playerManager pause];
}

- (void)close {
    if (self.status == WNPlayerStatusOpening) {
        self.nextOperation = WNPlayerOperationClose;
        return;
    }
    self.status = WNPlayerStatusClosing;
    [UIApplication sharedApplication].idleTimerDisabled = NO;
    [self.playerManager close];
}
#pragma mark ---------------------------- Private

- (void)openWithConfig:(WNPlayerConfig *)config
{
    self.config = config;
    if (self.status == WNPlayerStatusClosing) {
        self.nextOperation = WNPlayerOperationOpen;
        return;
    }
    if (self.status != WNPlayerStatusNone &&
        self.status != WNPlayerStatusClosed) {
        return;
    }
    self.status = WNPlayerStatusOpening;
    [self.playerManager openSource:self.urlString config:config];
    if (config.hwaccel) {
        [self insertSubview:self.playerManager.openGLESDisplayView atIndex:0];
    }
    else{
        [self insertSubview:self.playerManager.openGLESDisplayView atIndex:0]; 
    }
}

- (BOOL)doNextOperation {
    if (self.nextOperation == WNPlayerOperationNone) {
        return NO;
    }
    switch (self.nextOperation) {
        case WNPlayerOperationOpen:
            [self openWithConfig:self.config];
            break;
        case WNPlayerOperationPlay:
            [self play];
            break;
        case WNPlayerOperationPause:
            [self pause];
            break;
        case WNPlayerOperationClose:
            [self close];
            break;
        default:
            break;
    }
    self.nextOperation = WNPlayerOperationNone;
    return YES;
}

#pragma mark - Notifications 进入后台
- (void)appDidEnterBackground:(NSNotification *)notif {
    if (self.playerManager.playing) {
        [self pause];
        if (self.config.restorePlayAfterAppEnterForeground) {
            self.restorePlay = YES;
        }
    }
}

- (void)appWillEnterForeground:(NSNotification *)notif {
    if (self.restorePlay) {
        self.restorePlay = NO;
        [self play];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[[self.viewStatusBar valueForKey:@"_statusBar"]valueForKey:@"_statusBar"] performSelector:@selector(setForegroundColor:) withObject:[UIColor whiteColor]];
    });
}
-(void)setIsFullScreen:(BOOL)isFullScreen
{
    _isFullScreen = isFullScreen;
    if (_isFullScreen) {
        //状态栏处理
        if (kStatusBarHeight > 24) {
            if(![self.controlView viewWithTag:ControlViewTag]) {
                self.viewStatusBar.hidden = NO;
                [self.controlView addSubview:self.viewStatusBar];
                self.viewStatusBar.frame = CGRectMake(0, -5, 2*self.controlView.frame.size.width-40, self.viewStatusBar.frame.size.height);
            }
        }
        else{
            
        }
    }
    else{
        if (kStatusBarHeight > 24) {
            self.viewStatusBar.hidden = YES;
            if ([self.controlView viewWithTag:ControlViewTag]) {
                [self.viewStatusBar removeFromSuperview];
            }
        }
        else{
            
        }
    }
    if (self.controlView&&[self.controlView respondsToSelector:@selector(playerIsFullScreen:)]) {
        [self.controlView performSelector:@selector(playerIsFullScreen:) withObject:@(_isFullScreen)];
    }
}
#pragma mark 更新配置
- (void)setConfig:(WNPlayerConfig *)config
{
    _config = config;
    if (_config.hwaccel) {
        
    }
    else{
        
    }
}
 
- (void)playerPlayComplete:(NSNotification *)notif {
    self.status = WNPlayerStatusEOF;
    if (self.controlView&&[self.controlView respondsToSelector:@selector(playerPlayComplete:)]) {
        [self.controlView performSelector:@selector(playerPlayComplete:) withObject:self];
    }
    if (self.config.repeat) {
        [self replay];
    }
    else{
        [self close];
    }
}

- (void)playerClosed:(NSNotification *)notif {
    self.status = WNPlayerStatusClosed;
    [self destroyTimer];
    [self doNextOperation];
}

- (void)playerOpened:(NSNotification *)notif {
    self.status = WNPlayerStatusOpened;
    if (self.controlView&&[self.controlView respondsToSelector:@selector(playerReadyToPlay:)]) {
        [self.controlView performSelector:@selector(playerReadyToPlay:) withObject:self];
    }
    [self createTimer];
    if (![self doNextOperation]) {
        if (self.config.autoplay) {
            [self play];
        }
    }
}

- (void)playerBufferStateChanged:(NSNotification *)notif {
    NSDictionary *userInfo = notif.userInfo;
    if (self.controlView && [self.controlView respondsToSelector:@selector(playerBufferStateChanged:)]) {
        [self.controlView performSelector:@selector(playerBufferStateChanged:) withObject:userInfo[WNPlayerNotificationBufferStateKey]];
    }
}

- (void)playerError:(NSNotification *)notif {
    NSDictionary *userInfo = notif.userInfo;
    NSError *error = userInfo[WNPlayerNotificationErrorKey];
    if ([error.domain isEqualToString:WNPlayerErrorDomainDecoder]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.status = WNPlayerStatusNone;
            self.nextOperation = WNPlayerOperationNone;
        });
        NSLog(@"Player decoder error: %@", error);
    }
    else if ([error.domain isEqualToString:WNPlayerErrorDomainAudioManager]) {
        NSLog(@"Player audio error: %@", error);
        // I am not sure what will cause the audio error,
        // if it happens, please issue to me
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:WNPlayerNotificationError object:self userInfo:userInfo];
    
    if (self.controlView&&[self.controlView respondsToSelector:@selector(playerError:)]) {
        [self.controlView performSelector:@selector(playerError:) withObject:error];
    }
    
}

- (void)createTimer {
    if (self.dispath_timer) {
        return;
    }
    self.dispath_timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_timer(self.dispath_timer, DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC, 0.02 * NSEC_PER_SEC);
    dispatch_source_set_event_handler(self.dispath_timer, ^{
        [self syncScrubber];
    });
    dispatch_resume(self.dispath_timer);
}
- (void)destroyTimer {
    if (self.dispath_timer == nil) {
        return;
    }
    dispatch_cancel(self.dispath_timer);
    self.dispath_timer = nil;
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"%s",__FUNCTION__);
}

@end
